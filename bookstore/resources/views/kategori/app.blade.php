@extends('master')
@section('content')
{{ $status or ' ' }}
<div class="panel panel-info">
	<div class="panel-heading">
		Data Penulis
		<div class="pull-right">
			<a href="{{ url('kategori/tambah')}}" class="btn btn-success btn-sm">Tambah Data</a>
		</div>
	</div>
	<div class="panel-body">
		<table class="table">
				<tr>
					<td>Deskripsi</td>
					<td>Aksi</td>
				</tr>
				@foreach($kategori as $Kategori)
					
				<tr>
					<td >{{ $Kategori->deskripsi }}</td>
					<td >
					
					<a href="{{url('kategori/edit/'.$Kategori->id)}}" class="btn btn-primary btn-sm">Edit</a>
					<a href="{{url('kategori/hapus/'.$Kategori->id)}}" class="btn btn-danger btn-sm">Hapus</a>
					</td>
				</tr>
				@endforeach
			</table>
	</div>
</div>
@endsection
