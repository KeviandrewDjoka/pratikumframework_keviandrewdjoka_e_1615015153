<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pembeli;
use App\Pengguna;
use App\Http\Requests;
class PembeliControllerk extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pembeli = Pembeli::all();
        return view('pembeli.app',compact('pembeli'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('pembeli.tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $input)
    {

        $this->validate($input,[
            'username'=>'required',
            'password'=>'required',
            'nama'=>'required',
            'no_telp'=>'required',
            'email'=>'required',
            'alamat'=>'required'
        ])  ;

        $pengguna=new Pengguna();
        $pengguna->username=$input->username;
        $pengguna->password=$input->password;
        $pengguna->level="village";
        $pengguna->save();

        $pembeli=new Pembeli();
        $pembeli->nama=$input->nama;
        $pembeli->no_telp=$input->no_telp;
        $pembeli->email=$input->email;
        $pembeli->alamat=$input->alamat;
        $pembeli->pengguna_id=$pengguna->id;
        $status = $pembeli->save();
        return redirect('pembeli');



        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pembeli = Pembeli::find($id);
        return view('pembeli.edit')-> with(array('pembeli'=>$pembeli));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $input)
    {
        $pembeli=Pembeli::find($id);
        $pembeli->nama=$input->nama;
        $pembeli->no_telp=$input->no_telp;
        $pembeli->email=$input->email;
        $pembeli->alamat=$input->alamat;
        $status = $pembeli->save();


        $pengguna=Pengguna::find($id);
        $pengguna->username=$input->username;
        $pengguna->password=$input->password;
        $pengguna->level="village";
        
        $pengguna->save();



        return redirect('pembeli')->with(['status'=>$status]);   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pembeli=Pembeli::find($id);
        $pembeli->delete();
        return redirect('pembeli');
    }
}
