<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Pengguna;

class Pembeli extends Model
{
     protected $table = 'pembeli';
    protected $fillable=['nama','no_telp','email','alamat','pengguna_id'];

    public function Pengguna()
    {
    	return $this->belongsTo(Pengguna::class);
    }

    


}
