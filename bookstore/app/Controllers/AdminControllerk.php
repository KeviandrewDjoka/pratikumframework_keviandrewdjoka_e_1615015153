<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;
use App\Http\Requests;
class AdminControllerk extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admin = Admin::all();
        return view('admin.app',compact('admin'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      
        
         return view('admin.tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $input)
    {
      $this->validate($input, array
(
'nama' => 'required',
'no_telp' => 'required',
'email'=> 'required',
'alamat' => 'required',
));


        $admin=new Admin();
        $admin->nama=$input->nama;
        $admin->no_telp=$input->no_telp;
        $admin->email=$input->email;
        $admin->alamat=$input->alamat;
        $admin->pengguna_id=$input->pengguna_id;
        $status = $admin->save();
        return redirect('admin');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $admin=Admin::find($id);
        return view ('admin.edit')->with(array('admin'=>$admin));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id,Request $input)
    {
        $admin=Admin::find($id);
        $admin->nama=$input->nama;
        $admin->no_telp=$input->no_telp;
        $admin->email=$input->email;
        $admin->alamat=$input->alamat;
        $admin->pengguna_id=$input->pengguna_id;
        $status = $admin->save();
        return redirect('admin')->with(['status'=>$status]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $admin=Admin::find($id);
        $admin->delete();
        return redirect('admin');
    }
}
