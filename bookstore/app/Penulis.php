<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Buku;
class Penulis extends Model
{
    protected $table = 'penulis';
    protected $fillable=['nama','no_telp','email','alamat'];


public function Buku(){
    return $this->belongsToMany(Buku::class);
	}

}
