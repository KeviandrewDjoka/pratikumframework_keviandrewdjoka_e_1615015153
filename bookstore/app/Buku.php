<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Kategori;
use App\Penulis;

class Buku extends Model
{
   protected $table = 'buku';
    protected $fillable=['judul','kategori_id','penerbit','tanggal'];
    
     public function Kategori()
    {
    	return $this->belongsTo(Kategori::class);
    }
public function Penulis(){
        return $this->belongsToMany(Penulis::class);
    }

    public function Pembeli(){
        return $this->belongsToMany(Pembeli::class);
    }
}
