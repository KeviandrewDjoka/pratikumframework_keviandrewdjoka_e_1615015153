<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Buku;

class Kategori extends Model
{
   protected $table = 'kategori';
    protected $fillable=['deskripsi'];
   
   public function Buku()
    {
    	return $this->hasMany(Buku::class);
    }
}
